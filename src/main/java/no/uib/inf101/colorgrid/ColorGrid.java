package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  private int rows;
  private int cols;

  CellPosition pos;

  List<List<Color>> grid = new ArrayList<List<Color>>();

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
  
    for (int i = 0; i < rows; i++) {
      List<Color> row = new ArrayList<Color>();
      for (int j = 0; j < cols; j++) {
        row.add(null);
      }
      this.grid.add(row);
    }

  }
  //Override method rows() of Interface GridDimension
  @Override
  public int rows() {
    return this.rows;
  }

  //Override method cols() of Interface GridDimension
  @Override
  public int cols() {
    return this.cols;
  }

  //Override method getCells() of interface CellColorCollection
  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellColor = new ArrayList<>();
    for(int i = 0; i < rows; i++) {
      for(int j = 0; j < cols; j++) {
        CellPosition pos = new CellPosition(i, j);
        cellColor.add(new CellColor(pos, get(pos)));
      }
    }

    return cellColor;
  }

  //Override method get(CellPosition pos) of interface IColorGrid
  @Override
  public Color get(CellPosition pos) {
    return this.grid.get(pos.row()).get(pos.col());
  }

  //Override method set(CellPosition pos, Color color) of interface IColorGrid
  @Override
  public void set(CellPosition pos, Color color) {
    this.grid.get(pos.row()).set(pos.col(), color);
  }

}
