package no.uib.inf101.gridview;

import javax.swing.JPanel;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.CellColorCollection;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class GridView extends JPanel {
  IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponents(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }

  private void drawGrid(Graphics2D g2) {
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D rectangle2D = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    g2.setColor(MARGINCOLOR);
    g2.fill(rectangle2D);

    CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(rectangle2D, grid, OUTERMARGIN);

    drawCells(g2, grid, cellPositionToPixelConverter);

  }

  private static void drawCells (Graphics2D g2, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPositionToPixelConverter) {
    List<CellColor> cellColorList = cellColorCollection.getCells();
    for (CellColor cellColor : cellColorList) {
      Rectangle2D rectangle2D = cellPositionToPixelConverter.getBoundsForCell(cellColor.cellPosition());
      Color color = cellColor.color();
      if (cellColor.color() == null) {
        color = Color.DARK_GRAY;
      }

      g2.setColor(color);
      g2.fill(rectangle2D);
    }

  }






}
