package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double rowMargins = (gd.cols() + 1) * margin;
    double cellWidth = (box.getWidth() - rowMargins) / gd.cols();
    double cellX = (box.getX() + margin) + (cellWidth + margin) * cp.col();

    double colMargins = (gd.rows() + 1) * margin;
    double cellHeight = (box.getHeight() - colMargins) / gd.rows();
    double cellY = (box.getY() + margin) + (cellHeight + margin) * cp.row();

    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
  };

  
  
}
