package no.uib.inf101.gridview;
import java.awt.Color;
import javax.swing.JFrame;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.CellPosition;

public class Main {
  public static void main(String[] args) {
    IColorGrid colorGrid = new ColorGrid(3, 4);
    //RED upper left corner (position (0, 0))
    colorGrid.set(new CellPosition(0,0), Color.RED);

    //BLUE upper right corner (position (0, 3))
    colorGrid.set(new CellPosition(0,3), Color.BLUE);

    //YELLOW lower left corner (position (2, 0))
    colorGrid.set(new CellPosition(2,0), Color.YELLOW);

    //GREEN lower right corner (position (2, 3))
    colorGrid.set(new CellPosition(2,3), Color.GREEN);

    GridView grid = new GridView(colorGrid);
    JFrame frame = new JFrame();
    frame.setContentPane(grid);
    frame.setTitle("INF101_LAB4");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

  }
}
